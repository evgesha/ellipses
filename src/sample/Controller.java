package sample;

import drawing.Ellipses;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Slider;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Контроллер вращения эллипсов
 */
public class Controller implements Initializable {
    @FXML
    private Slider ellips1;
    @FXML
    private Slider vertex2;
    @FXML
    private Slider ellips2;
    @FXML
    private Slider vertex3;
    @FXML
    private Slider ellips3;
    @FXML
    private Canvas canvas;

    boolean start;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        start = false;
    }

    public void onClicked() {
            if (!start) {
                start = false;
                Ellipses ellipses = new Ellipses(canvas.getGraphicsContext2D());
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(() -> {
                            ellipses.tickTimer(ellips1.getValue(), vertex2.getValue(), ellips2.getValue(), vertex3.getValue(), ellips3.getValue());
                        });
                    }
                }, 0, 100);
            }

    }

}
