package drawing


/**
 * Класс для работы над точками плоскости с перегруженными операторами
 */
class Point(var x: Double, var y: Double) {

    constructor() :this(0.0,0.0){
    }

    operator fun unaryMinus(): Point = Point(-this.x, -this.y)

    operator fun plus(right: Point): Point = Point(x + right.x, y + right.y)

    operator fun times(right: Double): Point = Point(x * right, y * right)

    operator fun times(matrix: AffineTransformation): Point {
        val point = arrayOf(this.x, this.y, 1.0)
        var result = arrayOf(0.0, 0.0, 0.0)
        for (i in 0..2) {
            for (j in 0..2) {
                result[i] += point[j]*matrix.matrix[j][i]
            }
        }
        return Point(result[0], result[1])
    }
}

operator fun Array<Point>.times(matrix: AffineTransformation): Array<Point> {
    var result = Array<Point>(this.size, { Point(0.0, 0.0) })
    for (i in 0 until this.size) {
        result[i] = this[i] * matrix
    }
    return result
}
