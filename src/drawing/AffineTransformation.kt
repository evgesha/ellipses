package drawing

import java.lang.Math.*

/**
 * Класс аффинных преобразований
 */
open class AffineTransformation {
    var matrix: Array<Array<Double>> = Array(3, { Array(3, { 0.0 }) })

    /**
     * Основная функция движения
     */
    operator fun times(right: AffineTransformation): AffineTransformation {
        var result = AffineTransformation()

        for (i in 0..2) {
            for (j in 0..2) {
                for (k in 0..2) {
                    result.matrix[i][j] += matrix[i][k] * right.matrix[k][j]
                }
            }
        }
        return result
    }
}

/**
 * Функция сдвига
 */
class T(point: Point) : AffineTransformation() {
    init {
        matrix[0][0] = 1.0
        matrix[1][1] = 1.0
        matrix[2][0] = point.x
        matrix[2][1] = point.y
        matrix[2][2] = 1.0
    }
}

/**
 * Функция масштабирования
 */
class S(point: Point) : AffineTransformation() {
    init {
        matrix[0][0] = point.x
        matrix[1][1] = point.y
        matrix[2][2] = 1.0
    }
}

/**
 * Функция поворота
 */
class R(a: Double) : AffineTransformation() {
    init {
        matrix[0][0] = cos(a)
        matrix[0][1] = -sin(a)
        matrix[1][0] = sin(a)
        matrix[1][1] = cos(a)
        matrix[2][2] = 1.0
    }
}
