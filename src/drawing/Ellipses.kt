package drawing

import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color
import java.util.*

/**
 * Основные операции над эллипсами
 */
class Ellipses(val graphics: GraphicsContext) {
    private var ellipse1: Array<Point>
    private var ellipse2: Array<Point>
    private var ellipse3: Array<Point>

    private var index1 = 0
    private var index2 = 0

    private var t2 = 0.0
    private var t3 = 0.0

    init {
        ellipse1 = сreateEllipse(300.0, 300.0, 100.0, 50.0)
        ellipse2 = сreateEllipse(ellipse1[0].x, ellipse1[0].y, 100.0 / 2, 50.0 / 2)
        ellipse3 = сreateEllipse(ellipse2[0].x, ellipse2[0].y, 100.0 / 4, 50.0 / 4)
    }

    /**
     * Отображение линии
     */
    fun drawLines(color: Color, points: Array<Point>) {
        graphics.stroke = color
        for (i in 0 until (points.size - 1)) {
            graphics.strokeLine(points[i].x, points[i].y, points[i + 1].x, points[i + 1].y)
        }
    }

    /**
     * Установка свойств для эллипсов
     */
    fun drawEllipse(color: Color, x0: Double, y0: Double, a: Double, b: Double) {
        graphics.stroke = color
        graphics.strokeOval(x0, y0, a, b)
    }

    /**
     * Создание эллипсов
     */
    fun сreateEllipse(x0: Double, y0: Double, a: Double, b: Double): Array<Point> {
        var ellipse: List<Point> = LinkedList<Point>()
        var x1 = a
        while (x1 >= -a) {
            val y1 = Math.sqrt(b * b - x1 * x1 / a / a * b * b)
            ellipse += (Point(x1 + x0, y1 + y0))
            x1--
        }
        var x2 = -a + 1
        while (x2 <= a) {
            val y2 = -Math.sqrt(b * b - x2 * x2 / a / a * b * b)
            ellipse += (Point(x2 + x0, y2 + y0))
            x2++
        }
        return ellipse.toTypedArray()
    }

    /**
     * Рассчет центра эллипсов
     */
    fun сenterEllipse(ellipse: Array<Point>): Point {
        var x = 0.0
        var y = 0.0
        for (element in ellipse) {
            x += element.x
            y += element.y
        }
        return Point(x / ellipse.size, y / ellipse.size)
    }

    /**
     * Основная функция вращения для выполнения в run
     */
    fun tickTimer(A1: Double, V2: Double, A2: Double, V3: Double, A3: Double) {

        if (t2 >= 1.0) {
            t2 = 0.0
            index1 = (index1 + 1) % (ellipse1.size - 1)
        }
        if (t2 < -1) {
            t2 = 0.0
            index1 = if (index1 == 0) (ellipse1.size - 2) else index1 - 1
        }
        if (t3 >= 1.0) {
            t3 = 0.0
            index2 = (index2 + 1) % (ellipse2.size - 1)
        }
        if (t3 < -1) {
            t3 = 0.0
            index2 = if (index2 == 0) (ellipse2.size - 2) else index2 - 1
        }
        drow(A1 / 30.0, t2, A2 / 30.0, t3, A3 / 30.0)

        t2 += V2 / 10.0
        t3 += V3 / 10.0
    }


    private fun drow(a1: Double, t2: Double, a2: Double, t3: Double, a3: Double) {
        graphics.clearRect(0.0, 0.0, 800.0, 700.0)
        val c1 = сenterEllipse(ellipse1)
        val c2 = сenterEllipse(ellipse2)
        val c3 = сenterEllipse(ellipse3)

        ellipse1 *= T(-c1) * R(a1) * T(c1)

        val L2 = ellipse1[index1] * (1 - t2) + ellipse1[index1 + 1] * t2
        ellipse2 *= T(-c2) * R(a2) * T(L2)

        val L3 = ellipse2[index2] * (1 - t3) + ellipse2[index2 + 1] * t3
        ellipse3 *= T(-c3) * R(a3) * T(L3)

        drawLines(Color.BLUE, ellipse1)
        drawLines(Color.ORANGE, ellipse2)
        drawLines(Color.GREEN, ellipse3)

        drawEllipse(Color.RED, c1.x, c1.y, 3.0, 3.0)
        drawEllipse(Color.RED, L2.x, L2.y, 3.0, 3.0)
        drawEllipse(Color.RED, L3.x, L3.y, 3.0, 3.0)
    }
}